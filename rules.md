# Rules

some rules for my music library on:

- how the directory structure should look
  - how the artists, albums and titles should be
  - where the files should be
- how the id3 tags should look

## the directory structure

- `<artist>/<album>/<title>.mp3` for albums and EPs
- `<artist>/Singles/<title>.mp3` for singles
- `<covering artist>/Singles/<title> (<original artist> Cover).mp3` for covers
- `<remixer artist>/Remixes/<title> (<original artists> feat. <featurings> Remix).mp3` for remixes
- due to filesystem limitations, all occuring `/` are changed to the similar character `⧸`

## artists, albums and titles

- since directory structure doesn't allow multiple artists, the main artist should be the deciding folder
  - if no main artist exists pick the one with already other songs in your library
- albums of multiple artists should be named `<album name> (with <other artist>)`
- titles should be:
  - Remixes `<title> (<original artists> feat. <featurings> Remix)`
    - `remix` and `REMIX` are disallowed
    - other variations like `Flip` `Mix` `Piano Remix` `Cover`
  - Multiple artists `<title> (with <other artist>)`
    - `w/` or similar are disallowed
  - Featurings `<title> (feat. <featured artist>)`
    - `()`, `.` and exact capitalization is required
  - Multiple artists with featurings are put into one `()`
    - `<title> (with <other artist> feat. <featured artists>)`
  - Remixes of covers `<title> (<original artist> Cover) (<covering artist> Remix)`
  - multiple artists of the same category in the title should be concatenated with ` & `

## id3 tags

- The `Artist` and `Album Artist` tags should match with the folder name artist
- The `Album` tag should match with the folder name for albums
- The `Album` of singles should be the file name:
  - excluding any Featurings and other Artists
  - including Remix and Cover declarations
- all songs should have a cover art embedded
- all songs in albums should have the same cover art
- all songs should have appropriate Genres
  - Multiple Genres are Separated with `null` characters
    - how? eyeD3, as a command line tool can't do it

## included featurings

- only if the featuring is included in the original song title
- producers are not included, i have decided
- multiple featurings will be concatenated with ` & `

## collaborations

- songs of multiple artist go into the artist folder of the album artist
- the other artist is included in the song title like so:
  - `Song Title (with Other Artist)`
  - not `w/` `w` always with `()`
- multiple artists will get concatenated with ` & `

## examples

- A, B and C composed a Single featuring D
  - `A/Singles/Title (with B and C feat. D).mp3`
- A and B composed a Single featuring C, which was Remixed by D and E featuring F
  - `D/Remixes/Title (A & B feat. C Remix) (with E feat. F).mp3`
- A and B composed a Single featuring C, which was Covered by D and E feat. F
  - `D/Singles/Title (A & B feat. C Cover) (with E feat. F).mp3`
- A composed a Single which was first Covered by C and then Remixed by E featuring F
  - `E/Remixes/Title (A Cover) (C Remix) (feat. F).mp3`

## playlists

- all songs must be in at least one playlist
- all songs in playlists must exist
