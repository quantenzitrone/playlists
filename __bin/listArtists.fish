#!/usr/bin/env fish

for i in */
    set files $i/*/*.mp3
    printf '% 3i : %s %s\n' \
        (count $files) \
        (echo $i | rev | cut -c 2- | rev) \
        (test -f $i/rss && echo '󰑫')
end | sort -h