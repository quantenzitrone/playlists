playlists = {
    'Aggressive Phonk': {
        'Aggressive Phonk',
    },
    'Trip In Multiverse': {
        'Chill Phonk',
        'Wave',
        'Hardwave',
        'Witch House',
        'Synthwave',
        'Chillstep',
    },
    'Chill Phonk': {
        'Chill Phonk',
    },
    'Cyberphonk': {
        'Aggressive Phonk',
        'Cyberpunk Dubstep',
    },
    'Metal': {
        'Metal',
        'Metalcore',
        'Nu Metal',
    },
    'Dubstep': {
        'Dubstep',
    },
    'Heavy Trap': {
        'Traprun',
        'Bass Trap',
        'Dark Traprun',
    },
    'Rave': {
        'Frenchcore',
        'Trance',
        'Psytrance',
        'Goa',
        'Progressive Psytrance',
    },
    'Lo-Fi': {
        'Lo-Fi',
        #'Boom Bap',
    },
    'Rap': {
        'Shadow Rap',
    },
    'Bounce': {
        'Bounce',
    },
    'Melodic Dubstep': {
        'Melodic Dubstep'
    },
    'Hard Bass': {
        'Hard Bass',
    },
    'Electro House': {
        'Electro House',
    },
}

knownGenres = {j for i in playlists.values() for j in i}.union(
    {
        'Athmospheric',
        'Ambient',
        'Phonk',
    }
)
