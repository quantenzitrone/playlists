import re


def deriveAlbum(title: str) -> str:
    "Returns the album name from the Title of a Single. Some Substrings get removed."
    regexes = list(
        map(
            lambda x: f"\\ \\({x}\\)",
            # 'example' -> ' (example)'
            [
                "Sped Up",
                "Instrumental",
                "Explicit",
                "Slowed & Reverb",
                "Radio Edit",
                "Bass Boosted",
                "Acoustic Version",
            ]
            # 'example -> ' (example .*?)'
            + list(
                map(
                    lambda x: f'{re.escape(x + " ")}.*?',
                    [
                        "with",
                        "feat.",
                        "prod.",
                        "Live",
                    ],
                )
            )
            # 'example -> ' (.*? example)'
            + list(
                map(
                    lambda x: f'.*?{re.escape(" " + x)}',
                    [
                        "Mix",
                    ],
                )
            ),
        )
    )
    return re.sub("|".join(regexes), "", title)


def escape(string: str) -> str:
    "escapes an artist album or title for use in paths"
    string = re.sub("^\\.", "․", string)
    string = re.sub("/", "⧸", string)
    return string


def unescape(string: str) -> str:
    "unescapes an artist album or title for use in paths"
    string = re.sub("․", ".", string)
    string = re.sub("⧸", "/", string)
    return string


def validTitle(title: str) -> bool:
    "checks whether the title follows all conventions"
    illegalSubstrings = list(
        map(
            re.escape,
            (
                [
                    a + b + c
                    for a in ["(", " "]
                    for b in [
                        "ft",
                        "Ft",
                        "FT",
                        "Feat",
                        "FEAT",
                        "featuring",
                        "Featuring",
                        "FEATURING",
                        "Prod",
                        "PROD",
                    ]
                    for c in [".", " "]
                ]
                + [a + b + " " for a in ["(", " "] for b in ["feat", "prod"]]
                + ["remix", "REMIX"]
                + [a + b for a in ["(", " "] for b in ["w_" "w/"]]
            ),
        )
    ) + ["^\\."]

    return not re.match("|".join(illegalSubstrings), title)


red = "\033[91m"
green = "\033[92m"
yellow = "\033[93m"
blue = "\033[94m"
magenta = "\033[95m"
cyan = "\033[96m"
bold = "\033[1m"
underline = "\033[4m"
reset = "\033[0m"
