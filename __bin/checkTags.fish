#!/usr/bin/env fish

set_color -o red;   echo -n "Differences between the "; set_color normal
set_color -uo red;  echo -n "folder structure"; set_color normal 
set_color -o red;   echo -n " and the "; set_color normal
set_color -uo red;  echo -n "id3 tags"; set_color normal
set_color -o red;   echo ":"; set_color normal
echo "- the folder structure replaces / with ⧸ and . (at the start) with ․"
echo "- Singles in the folder structure are in <Artist>/Singles, so the Album has to be guessed from the title"
echo "- Remixes are in <Artist>/Remixes, so the Album has to be guessed as well"
echo

for i in */*/*.mp3
    set tags "$(eyeD3 $i)"
    
    # artist & album artist
    set path "$(
        echo $i | cut -d '/' -f 1 | sed 's/⧸/\//g' | sed 's/․/./g'
    )"
    # artist
    set id3 "$(
        echo $tags | grep -E '^artist: ' | cut -c 9-
    )"
    if test $path != $id3
        printf "%s artist:\n%s%s%s     ≠     %s%s%s\n" $i\
            (set_color red) $path (set_color normal) \
            (set_color green) $id3 (set_color normal)
    end
    # album artist
    set id3 "$(
        echo $tags | grep -E '^album artist: ' | cut -c 15-
    )"
    if test $path != $id3
        printf "%s album artist:\n%s%s%s     ≠     %s%s%s\n" $i\
            (set_color red) $path (set_color normal) \
            (set_color green) $id3 (set_color normal)
    end


    # album
    set path "$(
        set folder (echo $i | cut -d '/' -f 2)
        if test "$folder" = "Singles" || test "$folder" = "Remixes"
            echo $i | cut -d '/' -f 3 | sed 's/\.mp3//g' |
                sed 's/ (with .*)//g' |
                sed 's/ (feat\. .*)//g' |
                sed 's/ (prod\. .*)//g' |
                sed 's/ (Live .*)//g' |
                sed 's/ (.* Mix)//g' |
                sed 's/ (Sped Up)//g' |
                sed 's/⧸/\//g' | sed 's/․/./g' |
                sed -E 's/ \((Instrumental|Explicit|Slowed \& Reverb|Radio Edit|Bass Boosted|Acoustic Version)\)//g'
        else
            echo $folder
        end
    )"
    set id3 "$(
        echo $tags | grep -E '^album: ' | cut -c 8-
    )"
    if test $path != $id3
        printf "%s album:\n%s%s%s     ≠     %s%s%s\n" $i\
            (set_color red) $path (set_color normal) \
            (set_color green) $id3 (set_color normal)
    end


    # title
    set path "$(
        echo $i | cut -d '/' -f 3 | sed 's/\.mp3//g' | sed 's/⧸/\//g' | sed 's/․/./g'
    )"
    set id3 "$(
        echo $tags | grep -E '^title: ' | cut -c 8-
    )"
    if test $path != $id3
        printf "%s title:\n%s%s%s     ≠     %s%s%s\n" $i\
            (set_color red) $path (set_color normal) \
            (set_color green) $id3 (set_color normal)
    end
end
