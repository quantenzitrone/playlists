#!/usr/bin/env fish

set_color -o red
echo "These songs paths contain 'illegal' substrings:"
set_color normal
echo

set illegalSubstrings \
    '\\(ft ' '\\(ft\\.' ' ft ' ' ft.' \
    '\\(Ft ' '\\(Ft\\.' ' Ft ' ' Ft.' \
    '\\(FT ' '\\(FT\\.' ' FT ' ' FT.' \
    '\\(feat ' ' feat ' \
    '\\(Feat ' '\\(Feat\\.' ' Feat ' ' Feat\\.'\
    '\\(FEAT ' '\\(FEAT\\.' ' FEAT ' ' FEAT\\.' \
    'remix' 'REMIX' \
    ' w_ ' '\\(w_' \
    '^\.' '/\.'

for song in {.,}*/{.,}*/{.,}*.mp3
    echo $song | grep -E (string join '|' $illegalSubstrings)
end
